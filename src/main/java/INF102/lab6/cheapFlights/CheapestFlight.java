package INF102.lab6.cheapFlights;

import java.util.List;

import INF102.lab6.graph.WeightedDirectedGraph;

public class CheapestFlight implements ICheapestFlight {


    @Override
    public WeightedDirectedGraph<City, Integer> constructGraph(List<Flight> flights) {
        throw new UnsupportedOperationException("Implement me :)");
    }

    @Override
    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) {
        throw new UnsupportedOperationException("Implement me :)");
    }

}
